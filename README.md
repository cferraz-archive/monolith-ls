# monolith-ls [![version](https://img.shields.io/npm/v/monolith-ls.svg)](https://www.npmjs.com/package/monolith-ls)

Toolkit shit for working with almost-pure functional livescript.

## Description

Everyday `Prelude` for working with livescript in a purer manner.

Please, never use this library at production.
(and, if possible, never use this library)
Unless, you're maintaining it, of course.

### Features

The many useful features it includes are:

* **[Prelude-ls](https://github.com/gkz/prelude-ls)**, but organized in a
Haskell fashion.
* **Point-Free Monads**, very fast, but not fantasy-land compatible;
* **Function Repeater**, not pure, but removes the need to manage loop state.
* **Immutable Structures**, livescript-friendly
[mori](https://github.com/swannodette/mori) wrapper. Everything curried :)
* **Curried Promises**, a [bluebird](https://github.com/petkaantonov/bluebird)
wrapper with some hacks.
* **More Candy**, some newer functions for `prelude-ls`.

### Future?

I'll explain them later, but, for now, the plans for the future are:

* **More Monad Types** will be added soon.
* **Action System** (also known as the `Effect` system).
* **Reactive Loop** with **Signals**.
* **No Documentation**, this is not a library for general use, read the code.

## License

    Copyright 2016 Christian Ferraz Lemos de Sousa

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

## NPM

[![NPM](https://nodei.co/npm/monolith-ls.png?downloads=true&downloadRank=true&stars=true)](https://nodei.co/npm/monolith-ls/)
