require!{
  '../core/adt':   adt
  './applicative': Applicative
  './chain':       Chain
}

module.exports = adt class Monad extends (adt.join Applicative, Chain)
  ap: (m) -> @chain (f) -> m.map f
  map: (f) -> m=@; m.chain (a) -> m.of f a
