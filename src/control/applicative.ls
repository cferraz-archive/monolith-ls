require!{
  '../core/adt' : adt
  './apply'     : Apply
  './functor'   : Functor
}

adt class Applicative extends (adt.join Functor, Apply)
  of: adt.needed

  map: (f) -> @of f .ap @
