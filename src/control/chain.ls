require!{
  '../core/adt' : adt
  './apply'     : Apply
}

module.exports = adt class Chain extends Apply
  chain: adt.needed

  ap: (m) -> @chain (f) -> if typeof f == \function then m.map f
  map: (f) -> @chain (a) ~> @of f a
