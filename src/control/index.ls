module.exports =
  Apply:       require './apply/index'
  Applicative: require './applicative/index'
  Chain:       require './chain/index'

  Monad:       require './monad'
  Monads:      require './monads'

  Extend:      require './extend/index'
  Comonad:     require './comonad/index'
