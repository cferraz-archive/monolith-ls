Monads = {}

require!{
  '../../core/adt': adt
  '../monad':       Monad
}

module.exports = adt! class Monads.Just extends Monad
  (@value) !->

  of: -> new Just it

  is-just: -> true

  is-nothing: -> false

  chain: (f) -> if (v = f @value)of? then v else @of v
