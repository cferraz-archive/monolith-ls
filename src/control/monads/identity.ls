Monads = {}

require!{
  '../../core/adt': adt
  '../monad':       Monad
}

module.exports = adt! class Monads.Identity extends Monad
  (@value) !->

  of: -> new Identity it
  chain: (f) -> if (v = f @value)of? then v else @of v
