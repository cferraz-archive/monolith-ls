Monads = {}

require!{
  '../../core/adt': adt
  '../monad':       Monad
}

module.exports = adt! class Monads.Nothing extends Monad
  !->

  of: -> Nothing

  is-just: -> false

  is-nothing: -> true

  chain: (f) -> @

# singletone monad to avoid memory consumption
Nothing = new Monads.Nothing!
