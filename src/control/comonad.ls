require!{
  '../core/adt' : adt
  '../functor'  : Functor
  '../extend'   : Extend
}

module.exports = adt class Comonad extends (adt.join Functor, Extend)
  extract: adt.needed
