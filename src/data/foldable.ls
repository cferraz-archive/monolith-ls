require!{
  '../core/adt' : adt
  './semigroup' : Semigroup
}

module.exports = adt class Foldable extends Semigroup
  reduce: adt.needed
