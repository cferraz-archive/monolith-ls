require!{
  '../core/adt' : adt
  './semigroup' : Semigroup
  './foldable'  : Foldable
}

module.exports = adt class Foldable extends (adt.join Semigroup, Fuctor)
  sequence: adt.needed

  traverse: (f, o) --> @map f .sequence o
