require!{
  '../core/adt' : adt
  './semigroup' : Semigroup
}

module.exports = adt class Monoid extends Semigroup
  empty: adt.needed
