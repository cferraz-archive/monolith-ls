Persistent = {}

require! {
  mori
  '../adt':       adt
  './collection': Persistent.Collection
}

module.exports = adt class Persistent.Associative extends Persistent.Collection

  -> ...

  assoc: -> @of mori.assoc @value, it
  assoc-many: -> @of mori.assoc @value, ...it

  dissoc: -> @of mori.dissoc @value, it
  dissoc-many: -> @of mori.dissoc @value, ...it
