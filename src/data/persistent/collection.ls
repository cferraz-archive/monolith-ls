require!{
  mori
  '../list/map': map
  '../adt':      adt
}

module.exports = adt class Collection

  -> ...

  __values: -> it |> map -> it.value

  equals: -> mori.equals @value, it

  hash: -> mori.hash @value

  conj: -> mori.conj @value, it

  conj-many: -> mori.conj @value, ...it

  into: -> mori.into @value, it

  get: -> mori.get @value, it

  get-in: -> mori.get-in @value, it

  has-key: -> mori.has-key @value, it

  find: -> mori.find @value, it

  nth: -> mori.nth @value, it

  assoc-in: (v, k) --> mori.assoc-in @value, k, v

  update-in: (k, f) --> mori.update-in @value, k f

  count: -> mori.count @value

  empty: -> @of mori.empty @value

  is-empty: -> mori.is-empty @value

  native: -> mori.to-js @value

  # type checks
  @is-list        = -> mori.is-list it
  @is-seq         = -> mori.is-seq it
  @is-vector      = -> mori.is-vector it
  @is-map         = -> mori.is-map it
  @is-set         = -> mori.is-set it
  @is-collection  = -> mori.is-collection it
  @is-sequential  = -> mori.is-sequential it
  @is-associative = -> mori.is-associative it
  @is-counted     = -> mori.is-counted it
  @is-indexed     = -> mori.is-indexed it
  @is-reduceable  = -> mori.is-reduceable it
  @is-seqable     = -> mori.is-seqable it
  @is-reversible  = -> mori.is-reversible it
