Persistent = {}

require! {
  mori
  '../adt':     adt
  './sequence': Persistent.Sequence
}

module.exports = adt class Persistent.Range extends Persistent.Sequence

  -> @value = mori.range it

  @of = -> new Persistent.Range it
  of: -> Persistent.Range.of it
