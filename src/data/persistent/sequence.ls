Persistent = {}

require! {
  mori
  '../adt':                adt
  '../foldable':           Foldable
  '../monoid':             Monoid
  '../../control/functor': Functor
  './map':                 Persistent.Map
  './collection':          Persistent.Collection
}

SequenceCollection = adt.join Monoid, Foldable, Functor, Persistent.Collection

module.exports = adt! class Persistent.Sequence extends SequenceCollection

  -> ...

  @cons = (v, c) --> @of mori.cons c, @value
  @to-sequence = -> @of mori.seq it

  to-array: -> mori.into-array @value

  distinct: -> @of mori.distinct @value

  first: -> mori.first @valeu
  last: -> mori.last @value
  rest: -> @of mori.rest @value

  reverse: -> @of mori.reverse @value
  flatten: -> @of mori.flatten @value

  concat: -> @of mori.concat @value, it.value
  concat-many: -> @of mori.concat @value, ...(@__values it)

  map: (f) -> @of mori.map f, @value
  map-many: (f, a) -> @of mori.map f, @value, ...(@__values it)

  concat-map: (f) -> @of mori.mapcat f, @value
  concat-map-many: (f, a) --> mori.mapcat f, @value, ...(@__values it)

  filter: (f) -> @of mori.filter f, @value

  remove: (f) -> @of mori.remove f, @value

  reduce: (i, f) --> @of mori.reduce f, i, @value
  reduce1: (f) -> @of mori.reduce f, @value

  reduce-tuple: (i, f) --> @of mori.reduce-KV f, i, @value
  reduce1-tuple: (f) -> @of mori.reduce-KV f, @value

  take: -> @of mori.take it, @value
  take-while: (f) -> @of mori.take-while f, @value

  drop: -> @of mori.drop it, @value
  drop-while: (f) -> @of mori.drop-while f, @value

  some: (f) -> @of mori.some f, @value
  every: (f) -> @of mori.every f, @value

  sort: -> @of mori.sort @value
  sort-with: (f) -> @of mori.sort f, @value
  sort-by: -> @of mori.sort-by it, @value
  sort-by-with: (k, f) --> @of mori.sort-by k, f, @value

  interpose: -> @of mori.interpose it, @value
  interleave: -> @of mori.interleave @value, it.value
  interleave-many: -> @of mori.interleave @value, ...(@__values it)

  iterate: (f) -> @of mori.iterate f, @value

  repeat: -> switch it?
    | true => @of mori.repeat it, @value
    | _    => @of mori.repeat @value

  repeatedly: (n) -> switch it?
    | true => @of mori.repeatedly it, @value
    | _    => @of mori.repeatedly @value

  partition: (n) -> @of mori.partition n, @value
  detailed-partition: (n, s, p) --> @of mori.partition n, s, p, @value
  partition-by: (f) -> @of mori.partition-by f, @value

  group-by: (f) -> @of mori.group-by f, @value
  zipmap: -> Persistent.Map.of mori.zipmap @value, it.value
