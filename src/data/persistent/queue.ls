Persistent = {}

require! {
  mori
  '../adt': adt
  './set':  Persistent.Set
}

module.exports = adt class Persistent.Queue extends Persistent.Set

  -> @value = mori.queue it

  @of = -> new Persistent.Queue it
  of: -> Persistent.Queue.of it
