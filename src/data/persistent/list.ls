Persistent = {}

require! {
  mori
  '../adt':     adt
  './sequence': Persistent.Sequence
}

module.exports = adt class Persistent.List extends Persistent.Sequence

  -> @value = if mori.is-list it then it else mori.list it

  @of = -> new Persistent.List it
  of: -> Persistent.List.of it

  peek: -> mori.peek @value

  pop: -> @of mori.pop @value
