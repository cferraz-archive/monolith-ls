Persistent = {}

require! {
  mori
  '../adt': adt
  './list': Persistent.List
}

module.exports = adt class Persistent.Set extends Persistent.List

  (val, @sorted=false) -> @value = switch
    | mori.is-set val => val
    | _ => if sort then mori.sorted-set val else mori.set val

  @of = -> new Persistent.Set it
  of: -> new Persistent.Set it, @sorted

  disj: -> @of mori.disj @value, it

  union: -> @of mori.union @value, it.value
  union-many: -> @of mori.union @value, ...(@__values it)

  intersection: -> @of mori.intersection @value, it.value
  intersection-many: -> @of mori.intersection @value, ...(@__values it)

  difference: -> @of mori.difference @value, it.value
  difference-many: -> @of mori.difference @value, ...(@__values it)

  is-subset: -> mori.is-subset @value, it.value
  is-superset: -> mori.is-superset @value, it.value
