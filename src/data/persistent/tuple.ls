Persistent = {}

require! {
  mori
  '../adt':        adt
  './associative': Persistent.Associative
}

module.exports = adt class Persistent.Map extends Persistent.Associative

  (val, @sorted=false) -> @value = switch
    | mori.is-set val => val
    | _ => if sort then mori.sorted-set val else mori.set val

  @of = -> new Persistent.Map it
  of: -> new Persistent.Map it, @sorted

  keys: -> mori.keys @value
  values: -> mori.vals @value

  merge: -> @of mori.merge @value, it.value
  merge-many: -> @of mori.merge @value, ...(@__values it)
