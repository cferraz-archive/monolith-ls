Persistent = {}

require! {
  mori
  '../adt':        adt
  './associative': Persistent.Associative
  './list':        Persistent.List
}

AssociativeList = adt.join Persistent.Associative, Persistent.List

module.exports = adt class Persistent.Vector extends AssociativeList

  -> @value = if mori.is-vector it then it else mori.vector it

  @of = -> new Persistent.Vector it
  of: -> Persistent.Vector.of it

  subvec: (a, b) --> mori.subvec @value, a, b

  slice: (a, b) --> mori.subvec @value, a, (a+b)

  from: -> mori.subvec @value, it

  to: -> mori.subvec @value, 0, it
