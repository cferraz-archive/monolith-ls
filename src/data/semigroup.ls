require!{
  '../core/adt' : adt
}

module.exports = adt class Semigroup
  concat: adt.needed
