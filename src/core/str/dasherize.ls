# convert camelCase to camel-case, and setJSON to set-JSON
module.exports = (str) ->
    str
      .replace /([^-A-Z])([A-Z]+)/g, (, lower, upper) ->
         "#{lower}-#{if upper.length > 1 then upper else upper.to-lower-case!}"
      .replace /^([A-Z]+)/, (, upper) ->
         if upper.length > 1 then "#upper-" else upper.to-lower-case!
