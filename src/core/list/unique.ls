module.exports = (xs) ->
  result = []
  for x in xs when x not in result
    result.push x
  result
