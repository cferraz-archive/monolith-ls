module.exports = (p, xs) -->
  len = xs.length
  return xs unless len
  i = 0
  while i < len and p xs[i]
    i += 1
  xs.slice 0 i
