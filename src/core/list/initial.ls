module.exports = (xs) ->
  return void unless xs.length
  xs.slice 0, -1
