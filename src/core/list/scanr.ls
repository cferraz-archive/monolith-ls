scan = require './scanl'

module.exports = (f, memo, xs) -->
  xs = xs.concat!.reverse!
  (scan f, memo, xs).reverse!
