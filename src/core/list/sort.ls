module.exports = (xs) ->
  xs.concat!.sort (x, y) ->
    if x > y
      1
    else if x < y
      -1
    else
      0
