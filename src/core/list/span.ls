take-while = require './take-while'
drop-while = require './drop-while'

module.exports = (p, xs) --> [(take-while p, xs), (drop-while p, xs)]
