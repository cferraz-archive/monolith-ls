module.exports = (f, xs) -->
  passed = []
  failed = []
  for x in xs
    (if f x then passed else failed).push x
  [passed, failed]
