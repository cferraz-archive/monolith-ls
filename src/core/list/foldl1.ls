foldl = require './foldl'

module.exports = (f, xs) --> fold f, xs.0, xs.slice 1
