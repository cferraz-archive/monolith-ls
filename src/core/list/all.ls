module.exports = (f, xs) -->
  for x in xs when not f x
    return false
  true
