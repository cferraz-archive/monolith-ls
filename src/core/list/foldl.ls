module.exports = (f, memo, xs) -->
  for x in xs
    memo = f memo, x
  memo
