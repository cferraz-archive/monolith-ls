module.exports = (xs) ->
  sum = 0
  for x in xs
    sum += x
  sum / xs.length
