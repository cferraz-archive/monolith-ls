module.exports = (f, xs) -->
  xs.concat!.sort (x, y) ->
    if (f x) > (f y)
      1
    else if (f x) < (f y)
      -1
    else
      0
