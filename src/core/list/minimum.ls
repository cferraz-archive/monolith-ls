module.exports = (xs) ->
  min = xs.0
  for x in xs.slice 1 when x < min
    min = x
  min
