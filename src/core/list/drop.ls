module.exports = (n, xs) -->
  if n <= 0
    xs
  else
    xs.slice n
