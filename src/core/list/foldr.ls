module.exports = (f, memo, xs) -->
  for x in xs by -1
    memo = f x, memo
  memo
