module.exports = (f, xs) -->
  max = xs.0
  for x in xs.slice 1 when (f x) > (f max)
    max = x
  max
