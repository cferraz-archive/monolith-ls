take = require './take'
drop = require './drop'

module.exports = (n, xs) --> [(take n, xs), (drop n, xs)]
