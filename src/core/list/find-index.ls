module.exports = (f, xs) -->
  for x, i in xs when f x
    return i
  void
