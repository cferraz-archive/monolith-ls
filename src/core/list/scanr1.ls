scan = require './scanl'

module.exports = (f, xs) -->
  return void unless xs.length
  xs = xs.concat!.reverse!
  (scan f, xs.0, xs.slice 1).reverse!
