module.exports = (xs) ->
  result = 1
  for x in xs
    result *= x
  result
