module.exports = (f, xs) -->
  seen = []
  for x in xs
    val = f x
    continue if val in seen
    seen.push val
    x
