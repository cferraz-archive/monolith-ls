module.exports = (f, ...xss) ->
  min-length = undefined
  for xs in xss
    min-length <?= xs.length
  [f.apply(null, [xs[i] for xs in xss]) for i til min-length]
