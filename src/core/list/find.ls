module.exports = (f, xs) -->
  for x in xs when f x
    return x
  void
