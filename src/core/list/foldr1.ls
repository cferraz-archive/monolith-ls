foldr = require './foldr'

module.exports = (f, xs) --> foldr f, xs[*-1], xs.slice 0, -1
