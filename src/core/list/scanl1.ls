scan = require './scanl'

module.exports = (f, xs) -->
  return void unless xs.length
  scan f, xs.0, xs.slice 1
