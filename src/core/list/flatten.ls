# NOTE: dangerous recursive call here
flatten = (xs) ->
  [].concat.apply do
    []
    [(if typeof! x is 'Array' then flatten x else x) for x in xs]

module.exports = flatten
