module.exports = (f, xs) -->
  min = xs.0
  for x in xs.slice 1 when (f x) < (f min)
    min = x
  min
