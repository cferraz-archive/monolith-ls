module.exports = (xs) ->
  max = xs.0
  for x in xs.slice 1 when x > max
    max = x
  max
