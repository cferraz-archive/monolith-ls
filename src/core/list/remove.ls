elem-index = require './elem-index'

module.exports = (el, xs) -->
  i = elem-index el, xs
  xs.slice!
    ..splice i, 1 if i?
