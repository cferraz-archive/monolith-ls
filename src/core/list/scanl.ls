module.exports = (f, memo, xs) -->
  last = memo
  [memo] ++ [last = f last, x for x in xs]
