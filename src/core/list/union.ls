module.exports = (...xss) ->
  results = []
  for xs in xss
    for x in xs
      results.push x unless x in results
  results
