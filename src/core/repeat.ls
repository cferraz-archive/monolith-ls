# enables tail recursive logic (works more like a functional while method)
# fully recursive logic is almost impossible on js without going async
repeat = (fn) ->
  return (...init-args) ->
    stop = false
    args = [].concat init-args

    var output

    recur = ->
      output := it
      stop   := false

    recur.stop = ->
      output := it
      stop   := true

    while stop == false
      stop := true

      fn ...args, recur

      if stop == false and output?
        if typeof! output == \Array
        then args := output
        else args := [output]

    output

module.exports = repeat
