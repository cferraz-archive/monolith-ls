module.exports = (fn, data) -->
  has-mutable = data._mutable?
  mutable     = if has-mutable then data._mutable else data

  keys = Object.keys data
  neo-data    = {}
  neo-mutable = {}

  for v, k in data
    neo-data[k]    = v
    neo-mutable[k] = v

  fn neo-data

  Object.define-property neo-data \_mutable, __proto__: null, value: neo-mutable
  Object.freeze Object.seal neo-data

  return neo-data
