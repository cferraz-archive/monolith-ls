module.exports = (f, object) -->
  passed = {}
  failed = {}
  for k, x of object
    (if f x then passed else failed)[k] = x
  [passed, failed]
