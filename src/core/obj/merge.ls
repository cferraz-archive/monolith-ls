keys   = require './keys'
unique = require '../Array/unique'

module.exports = (l, r) -->
  b-keys = unique (keys l) ++ (keys r)
  {[k, (l[k] or r[k])] for k in b-keys}
