module.exports = (data) ->
  neo-data    = {}
  neo-mutable = {}

  for v, k in data
    neo-data[k] = v
    neo-mutable[k] = v

  Object.defineProperty neo-data \_mutable, __proto__: null, value: neo-mutable
  Object.freeze Object.seal neo-data

  return neo-data
