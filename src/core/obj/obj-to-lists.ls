module.exports = (object) ->
  keys = []
  values = []
  for key, value of object
    keys.push key
    values.push value
  [keys, values]
