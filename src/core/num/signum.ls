module.exports = (x) ->
  if x < 0
    -1
  else if x > 0
    1
  else
    0
