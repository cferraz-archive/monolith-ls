module.exports = α =
  | process? => process.next-tick
  | setImediate? => setImediate
  |_ => -> setTimeout it, Infinity
