unique  = require './list/unique'
each    = require './list/each'
map     = require './list/map'
flatten = require './list/flatten'
curry   = require './func/curry'
filter  = require './list/filter'
foldl   = require './list/foldl'

# forbiden property names
forbid = <[
  __adt_keys
  __adt_proto_keys
  __type_join
  displayName
  superclass
  constructor
  isA
]>

###
# adt applies monolith's adt standard to commom livescript classes
###
module.exports = adt = (klass) ->
  # called with a bang? validate first
  unless klass? then return adt-validate

  # enumerate static keys
  adt-k = klass.__adt_keys or []
  adt-k = unique adt-k.concat Object.keys klass
  adt-k = adt-k |> filter -> (forbid.index-of it) == -1
  klass.__adt_keys = adt-k

  # enumerate prototype keys
  adt-p-k = klass.__adt_proto_keys or []
  adt-p-k = unique adt-p-k.concat Object.keys klass::
  adt-p-k = adt-p-k |> filter -> (forbid.index-of it) == -1
  klass.__adt_proto_keys = adt-p-k

  # set adt.needed functions ready to throw
  klass.__adt_keys |> each (k) ->
    if klass[k] == adt.needed
    then klass[k] = klass[k] klass.display-name, k

  # set adt.needed prototype functions ready to throw (and point-free helpers)
  klass.__adt_proto_keys |> each (k) ->
    # partially applied needed functions
    if klass::[k] == adt.needed
      klass::[k] = klass::[k] klass.display-name, "::#k"
    else unless klass::[k].__adt_needed
      klass[k] = curry klass::[k].lenght, (...args) -> klass::[k] ...args

  # instanceof won't work with multiple inheritance, use this instead
  klass.is = (t) ->
    switch
    # strict equality
    | klass == t => true
    # constructor equality
    | klass@@ == t => true
    # shares some of the joined types
    | -1 != klass.__type_join?index-of t => true
    # superclass has some of the joined types
    | klass.superclass?is? => klass.superclass.is t
    # is not a t type
    | _ => false

  # instance alias
  klass::is = (t) -> @@@is-of t

  # the cake is ready honey
  return klass


###
# private validation tool
###
adt-validate = (klass) ->
  klass = adt klass

  /*
  klass.__adt_keys |> each (k) ->
    if klass[k].__adt_needed? and klass[k].__adt_needed
    then klass[k]!

  klass.__adt_proto_keys |> each (k) ->
    if klass::[k].__adt_needed? and klass::[k].__adt_needed
    then klass::[k]!
  */

  return klass


###
# type error for classes that act but without implementing
###
adt.needed = (ʹfrom, name) ->
  f = -> throw TypeError "#{ʹfrom} needs function #{name}"
  f.__adt_needed = true
  return f

###
# creates a tree of dependences
###
dependence-tree = (ʹklass, acc=[]) ->
  if ʹklass.__type_join?
    ʹacc = ʹklass.__type_join |> map (ʺklass) ->
      # please don't blow the stack D:
      ʺacc = recursive-walk ʺklass, [acc, ʺklass]
      [acc, ʺklass, ʺacc]
    return [acc, ʹacc, ʹklass]
  else return [acc, ʹklass]

###
# join many classes into a inheratable one (reminder: instanceof won't work)
###
adt.join = (...classes) ->
  # a new cake
  klass = ->
  klass.__adt_keys       = []
  klass.__adt_proto_keys = []
  klass.__type_join      = flatten (classes |> map -> dependence-tree it)

  keys = -> Object.keys it

  # fold many classes from left to right
  fold-classes = foldl (c, n) ->
    ʹk = unique (keys n .concat (n.__adt_keys or []))
    ʺk = unique (keys n .concat (n.__adt_proto_keys or []))

    ʹk |> each (k) -> if -1 != forbid.index-of k
      if !c[k]? or (c[k]? and c[k].__adt_needed)
      then c[k] = n[k]

    ʺk |> each (k) -> if -1 != forbid.index-of k
      if !c[k]? or (c[k]? and c[k].__adt_needed)
      then c[k] = n[k]

    return c

  # cake is ready
  return fold-classes klass, classes
