module.exports =
  apply:     require './apply'
  curry:     require './curry'
  flip:      require './flip'
  fix:       require './fix'
  over:      require './over'
  memoize:   require './memoize'
