memoize = M = (f) ->
  memo = {}
  (...args) ->
    key = [arg + typeof! arg for arg in args].join ''
    memo[key] = if key of memo then memo[key] else f ...args

module.exports = memoize
