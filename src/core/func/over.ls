# Combines two functions: (f, g, x, y) l -> f (g x), (g y)
# Looks nice when applied infix.
module.exports = (f, g, x, y) --> f (g x), (g y)
