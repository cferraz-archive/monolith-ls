module.exports = (bound_, length_, fx_) ->
  | fx_? =>
    fx     = fx_
    length = length_
    bound  = bound_
  | length_? =>
    if \number == typeof bound_
      fx     = length_
      length = bound_
      bound  = void
    else
      fx     = length_
      length = fx.length
      bound  = bound_
  | bound_? =>
    fx     = bound_
    length = fx.length
    bound  = void

  fn = if bound?
    if bound == true
      fx.bind this
    else if !!bound
      fx.bind bound
    else fx
  else fx

  if length > 9
  then throw new Error "Trying to curry too many arguments (max: 9)."
  else switch length
    | 0 => fn
    | 1 => fn
    | 2 => curry2 fn, length
    | 3 => curry3 fn, length
    | 4 => curry4 fn, length
    | 5 => curry5 fn, length
    | 6 => curry6 fn, length
    | 7 => curry7 fn, length
    | 8 => curry8 fn, length
    | 9 => curry9 fn, length

curry2 = (fn, length) -> (z0, z1) ->
  | arguments.length is 2 => fn z0, z1
  |_ => (a) -> fn z0, a

curry3 = (fn, length) -> (z0, z1, z2) ->
  | arguments.length is 3 => fn z0, z1, z2
  |_ => switch length - arguments.length
    | 1 =>        (a) -> fn z0, z1, a
    | 2 => curry2 (a,b) -> fn z0,  a,b

curry4 = (fn, length) -> (z0, z1, z2, z3) ->
  | arguments.length is 4 => fn z0, z1, z2, z3
  |_ => switch length - arguments.length
    | 1 =>        (a) -> fn z0, z1, z2, a
    | 2 => curry2 (a,b) -> fn z0, z1,  a,b
    | 3 => curry3 (a,b,c) -> fn z0,   a,b,c

curry5 = (fn, length) -> (z0, z1, z2, z3, z4) ->
  | arguments.length is 5 => fn z0, z1, z2, z3, z4
  |_ => switch length - arguments.length
    | 1 =>        (a) -> fn z0, z1, z2, z3, a
    | 2 => curry2 (a,b) -> fn z0, z1, z2,  a,b
    | 3 => curry3 (a,b,c) -> fn z0, z1,   a,b,c
    | 4 => curry4 (a,b,c,d) -> fn z0,    a,b,c,d

curry6 = (fn, length) -> (z0, z1, z2, z3, z4, z5) ->
  | arguments.length is 6 => fn z0, z1, z2, z3, z4, z5
  |_ => switch length - arguments.length
    | 1 =>        (a) -> fn z0, z1, z2, z3, z4, a
    | 2 => curry2 (a,b) -> fn z0, z1, z2, z3,  a,b
    | 3 => curry3 (a,b,c) -> fn z0, z1, z2,   a,b,c
    | 4 => curry4 (a,b,c,d) -> fn z0, z1,    a,b,c,d
    | 5 => curry5 (a,b,c,d,e) -> fn z0,     a,b,c,d,e

curry7 = (fn, length) -> (z0, z1, z2, z3, z4, z5, z6) ->
  | arguments.length is 7 => fn z0, z1, z2, z3, z4, z5, z6
  |_ => switch length - arguments.length
    | 1 =>        (a) -> fn z0, z1, z2, z3, z4, z5, a
    | 2 => curry2 (a,b) -> fn z0, z1, z2, z3, z4,  a,b
    | 3 => curry3 (a,b,c) -> fn z0, z1, z2, z3,   a,b,c
    | 4 => curry4 (a,b,c,d) -> fn z0, z1, z2,    a,b,c,d
    | 5 => curry5 (a,b,c,d,e) -> fn z0, z1,     a,b,c,d,e
    | 6 => curry6 (a,b,c,d,e,f) -> fn z0,      a,b,c,d,e,f

curry8 = (fn, length) -> (z0, z1, z2, z3, z4, z5, z6, z7) ->
  | arguments.length is 8 => fn z0, z1, z2, z3, z4, z5, z6, z7
  |_ => switch length - arguments.length
    | 1 =>        (a) -> fn z0, z1, z2, z3, z4, z5, z6, a
    | 2 => curry2 (a,b) -> fn z0, z1, z2, z3, z4, z5,  a,b
    | 3 => curry3 (a,b,c) -> fn z0, z1, z2, z3, z4,   a,b,c
    | 4 => curry4 (a,b,c,d) -> fn z0, z1, z2, z3,    a,b,c,d
    | 5 => curry5 (a,b,c,d,e) -> fn z0, z1, z2,     a,b,c,d,e
    | 6 => curry6 (a,b,c,d,e,f) -> fn z0, z1,      a,b,c,d,e,f
    | 7 => curry7 (a,b,c,d,e,f,g) -> fn z0,       a,b,c,d,e,f,g

curry9 = (fn, length) -> (z0, z1, z2, z3, z4, z5, z6, z7, z8) ->
  | arguments.length is 9 => fn z0, z1, z2, z3, z4, z5, z6, z7, z8
  |_ => switch length - arguments.length
    | 1 =>        (a) -> fn z0, z1, z2, z3, z4, z5, z6, z7, a
    | 2 => curry2 (a,b) -> fn z0, z1, z2, z3, z4, z5, z6,  a,b
    | 3 => curry3 (a,b,c) -> fn z0, z1, z2, z3, z4, z5,   a,b,c
    | 4 => curry4 (a,b,c,d) -> fn z0, z1, z2, z3, z4,    a,b,c,d
    | 5 => curry5 (a,b,c,d,e) -> fn z0, z1, z2, z3,     a,b,c,d,e
    | 6 => curry6 (a,b,c,d,e,f) -> fn z0, z1, z2,      a,b,c,d,e,f
    | 7 => curry7 (a,b,c,d,e,f,g) -> fn z0, z1,       a,b,c,d,e,f,g
    | 8 => curry8 (a,b,c,d,e,f,g,h) -> fn z0,        a,b,c,d,e,f,g,h
