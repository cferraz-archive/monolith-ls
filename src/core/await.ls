# NEEDS REFACTORING
α = require './async'

module.exports = $ = (f) ->
  tasks = []

  out = (cb) ->
    if out.finished
      if cb.length == 2
      then return $ (done) !-> cb out.value, done
      else cb out.value
    else
      if cb.length == 2
      then return $ (done) !-> out (val) -> cb val, done
      else tasks.push cb
    return void

  out.value    = null
  out.finished = false
  out.async    = true

  α !-> f (value) !->
    <-! flatten value

    out.value    := it
    out.finished := true

    # lazify this later
    while tasks.length > 0 then tasks.shift! out.value

  out

flatten = (value, done) !->
  switch
  | value? and typeof! value.value == \Function =>
    α !-> flatten value.value, done
  | typeof! value == \Function and value.async? =>
    value !-> flatten it, done
  | _ => done value
