module.exports = (fn, n) -->
  if n <= 0 then return []

  acc = []
  i   = 0

  while i < n
    acc[i-1] = fn i
    i++

  return acc
