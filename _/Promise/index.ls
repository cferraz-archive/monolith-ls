Promise = require \bluebird

# better constructor
P = (fn) -> new Promise (resolve, reject) !->
  switch fn.length
  | 0 => reject new Error 'Nothing to be done'
  | 1 => fn(-> if typeof! it == \Error then reject it else resolve it)
  | 2 => fn resolve, reject
  | _ => reject new Error 'Too many arguments within Promise constructor.'

P.seq = (...promises) -> (previous) ->
  if previous instanceof P.promise
    value <- previous.then
    promises |> P.reduce value, (prev, curr) -> curr prev
  else promises |> P.reduce previous, (prev, curr) -> curr prev

# single argument functions
P.promise = Promise
P.join    = Promise.join
P.try     = Promise.try
P.method  = Promise.method
P.resolve = Promise.resolve
P.reject  = Promise.reject
P.all     = Promise.all
P.any     = Promise.any
P.race    = Promise.race
P.props   = Promise.props
P.using   = Promise.using

# promisification (creates effects)
P.promisify     = Promise.promisify
P.promisify-all = Promise.promisify-all
P.from-callback = Promise.fromCallback

# side-effect free (?) functions
P.some       = (count, input) --> Promise.any input, count
P.map        = (mapper, input) --> Promise.map input, mapper
P.reduce     = (value, reducer, input) --> Promise.reduce input, reducer, value
P.filter     = (filterer, input) --> Promise.filter input, filterer
P.each       = (iterator, input) --> Promise.each input, iterator
P.map-series = (mapper, input) --> Promise.map-series input, mapper
P.schedule   = (fn) -> Promise.set-scheduler fn

module.exports = P
