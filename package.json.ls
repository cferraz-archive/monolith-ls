name: 'monolith-ls'
version: '0.3.0'
author: 'Christian Ferraz Lemos de Sousa <cferraz95@gmail.com>'
description: 'Experimental fork of `prelude-ls`, gives a more functional feel to prelude. Never use this on production as it has tons of misconceptions about functional programming like monads, function purity and recursion.'
keywords: <[
  prelude'
  livescript'
  utility'
  ls'
  library'
  functional'
  array'
  list'
  object'
  string'
  async'
  monad'
  pure
]>

files: <[
  lib/*.js
  README.md
  LICENSE
]>

homepage: 'https://github.com/masterrace/monolith-ls#readme'
bugs: url: 'https://github.com/masterrace/monolith-ls/issues'
license: 'Apache 2.0'

engines:
  node: '>= 0.8.0'

repository:
  type: 'git'
  url: 'git+https://github.com/masterrace/monolith-ls.git'

peer-dependencies:
  mori:        '^0.3.2'
  async:       '^1.5.2'
  bluebird:    '^3.1.1'
  'cls-tools': '^1.1.1'

dev-dependencies:
  benchmark:   '^2.0.0'
  mocha:       '^2.3.4'
  sinon:       '^1.17.2'
  'custom-ls': "1.4.0-13"
