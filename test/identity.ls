require! {
  assert: {strict-equal: eq, deep-equal: deep-eq, ok}

  './laws/functor':     F
  './laws/apply':       A
  './laws/applicative': Apv
  './laws/chain':       C
  './laws/monad':       M

  # monads
  '../lib/control/monads/identity': Identity
}

It = it

describe 'Identity' ->
  f = (x) -> Identity.of x * 4
  g = (x) -> Identity.of x / 3

  var m

  It 'should instantiate' -> eq (m := new Identity 3)value, 3

  context 'Monad laws' ->

    It 'should left-identity' ->
      eq ...M.left-identity 3, Identity, f

    It 'should right-identity' ->
      eq ...M.right-identity m

  context 'Chain laws' ->

    It 'should be associative' ->
      eq ...C.associativity m, f, g

  context 'Applicative laws' ->

    f = (x) -> x.chain (y) -> y*2

    It 'should pass identity' ->
      eq ...Apv.identity Identity, m

    It 'should be homomorphic' ->
      eq ...Apv.homomorphism Identity, f, m

    It 'should be interchangeable' ->
      eq ...Apv.interchange Identity, (Identity.of f), m

  context 'Apply laws' ->

    It 'should be composable' ->
      u = Identity.of (x) -> 2
      a = Identity.of (x) -> x*2
      v = Identity.of (x) -> x
      eq ...A.composition a, u, v

  context 'Functor laws' ->

    It 'should pass identity' ->
      eq ...F.identity m

    It 'should be composable' ->
      by4 = (x) -> x * 4
      to3 = (x) -> x / 3
      eq ...F.composition m, by4, to3
