# IMPROVE THIS SECTION LATER
require! {
  assert: {strict-equal: eq, deep-equal: deep-eq, ok}

  './laws/functor':     F
  './laws/apply':       A
  './laws/applicative': Apv
  './laws/chain':       C
  './laws/monad':       M

  # monads
  '../lib/control/monads/maybe': {Just, Nothing}
}

It = it

describe 'Maybe' ->
  f = (x) -> Just.of x * 4
  g = (x) -> Just.of x / 3

  var m

  It 'should instantiate' -> eq (m := new Just 3)value, 3

  context 'Nothing propagation' ->
    a = Just.of 10

    var four
    var nothing

    by2-when-pair = (x) -> if x%2 == 0 then Just.of x * 2 else new Nothing

    It 'should instantiate' ->
      four := by2-when-pair 2
      nothing := by2-when-pair 3

    It 'should reach the end of the computation' ->
      val = (a.chain (x) -> four.chain (y) -> by2-when-pair x+y)value
      eq 28, val

    It 'should not reach the end of the computation' ->
      val = (a.chain (x) -> nothing.chain (y) -> by2-when-pair x+y)
      eq val.is-nothing!, true

  context 'Monad laws' ->

    It 'should left-identity' ->
      eq ...M.left-identity 3, Just, f

    It 'should right-identity' ->
      eq ...M.right-identity m

  context 'Chain laws' ->

    It 'should be associative' ->
      eq ...C.associativity m, f, g

  context 'Applicative laws' ->

    f = (x) -> x.chain (y) -> y*2

    It 'should pass identity' ->
      eq ...Apv.identity Just, m

    It 'should be homomorphic' ->
      eq ...Apv.homomorphism Just, f, m

    It 'should be interchangeable' ->
      eq ...Apv.interchange Just, (Just.of f), m

  context 'Apply laws' ->

    It 'should be composable' ->
      u = Just.of (x) -> 2
      a = Just.of (x) -> x*2
      v = Just.of (x) -> x
      eq ...A.composition a, u, v

  context 'Functor laws' ->

    It 'should pass identity' ->
      eq ...F.identity m

    It 'should be composable' ->
      by4 = (x) -> x * 4
      to3 = (x) -> x / 3
      eq ...F.composition m, by4, to3
