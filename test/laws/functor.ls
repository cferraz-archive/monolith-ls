module.exports =

  identity: (u) ->
    [
      * (u.map (a) -> a)value
      * u.value
    ]

  composition: (u, g, f) ->
    [
      * (u.map (x) -> f g x)value
      * u.map g .map f .value
    ]
