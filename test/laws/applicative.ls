module.exports =

  identity: (a, v)->
    [
      * (a.of (x) -> x)ap(v).value
      * v.value
    ]

  homomorphism: (a, f, x) ->
    [
      * a.of f .ap a.of x .value.value
      * a.of f x .value.value
    ]

  interchange: (a, u, y) ->
    [
      * u.ap a.of y .value.value
      * (a.of (f) -> f y)ap(u)value.value
    ]
