module.exports =

  associativity: (m, f, g) ->
    [
      * m.chain f .chain g .value
      * (m.chain (x) -> f x .chain g)value
    ]
