module.exports =

  left-identity: (a, m, f) ->
    [
      * m.of a .chain f .value
      * f a .value
    ]

  right-identity: (m) ->
    [
      * m.chain m.of .value
      * m.value
    ]
