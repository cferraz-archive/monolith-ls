Identity = require '../source/Control/Monad/Identity'

a = Identity.of 2
b = Identity.of 3
c = a.chain (x) -> b.chain (y) -> Identity.of x * y

c.chain -> console.log '2 * 3:', it

d = Identity.do ({chain}) ->
  x <- chain 6
  y <- chain 2
  x / y

d.chain -> console.log '6 / 2:', it

e = Identity.do ->
  x <: a
  y <: b
  x ^ y

e.chain -> console.log '2 ^ 3:', it

f = a >: (x) -> b >: (y) -> Identity.of x + y

f.chain -> console.log '2 + 3:', it
