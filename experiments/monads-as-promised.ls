# this is a example of how to join promises and monads, please remember that
# promises are already monads, use "await" monads intead.

P        = require '../source/Control/Promise'
Identity = require '../source/Control/Monad/Identity'

a = Identity.of P (pass) -> pass 2
b = Identity.of P (pass) -> pass 3
c = a.chain (x) -> b.chain (y) ->
  Identity.of P (pass) ->
    x.then (x) -> y.then (y) -> pass x * y

c >: (z) -> z.then -> console.log '2 * 3:', it

d = Identity.do ({chain}) ->
  pass <- P
  x <- chain P (pass) -> pass 6
  y <- chain P (pass) -> pass 2
  x <- x.then
  y <- y.then
  pass x / y

d >: (z) -> z.then -> console.log '6 / 2:', it

e = Identity.do -> P (pass) ->
  x <: a
  y <: b
  x <- x.then
  y <- y.then
  pass x ^ y

e >: (z) -> z.then -> console.log '2 ^ 3:', it

f = a >: (x) -> b >: (y) ->
  Identity.of P (pass) ->
    x.then (x) -> y.then (y) -> pass x + y

f >: (z) -> z.then -> console.log '2 + 3:', it
