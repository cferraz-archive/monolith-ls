# using await alongside with monads is a simple way to make async monad systems
# (and it's lighter than using promises)
$        = require '../source/Core/Lambda/await'
Identity = require '../source/Control/Monad/Identity'

a = Identity.of $ (pass) -> pass 2
b = Identity.of $ (pass) -> pass 3
c = a.chain (x) -> b.chain (y) ->
  Identity.of $ (pass) ->
    x (x) -> y (y) -> pass x * y

c >: (z) -> z -> console.log '2 * 3:', it

d = Identity.do ({chain}) ->
  pass <- $
  x <- chain $ (pass) -> pass 6
  y <- chain $ (pass) -> pass 2
  x <- x
  y <- y
  pass x / y

d >: (z) -> z -> console.log '6 / 2:', it

e = Identity.do -> $ (pass) ->
  x <: a
  y <: b
  x <- x
  y <- y
  pass x ^ y

e >: (z) -> z -> console.log '2 ^ 3:', it

f = a >: (x) -> b >: (y) ->
  Identity.of $ (pass) ->
    x (x) -> y (y) -> pass x + y

f >: (z) -> z -> console.log '2 + 3:', it
