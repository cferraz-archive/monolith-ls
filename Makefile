default: all

SRC = $(shell find src -name "*.ls" -type f | sort)
PKG = $(SRC:src/%.ls=lib/%.js)

TEST = $(shell find test -name "*.ls" -type f | sort)
TOUT = $(TEST:test/%.ls=test/%.js)

CLSC = ./node_modules/.bin/lsc
MOCHA = ./node_modules/.bin/mocha

lib:
	mkdir -p lib/

lib/%.js: src/%.ls
	$(CLSC) --output "$(shell dirname $@)" --no-header -m linked --bare --compile "$<"

test/%.js: test/%.ls
	$(CLSC) --output "$(shell dirname $@)" --no-header --bare --compile "$<"

package.json: package.json.ls
	$(CLSC) --output . --compile ./package.json.ls

build: $(PKG) package.json lib

build-test: $(TOUT)

all: build

dev-install:
	npm install lib

loc:
	wc --lines src/*

clean:
	rm -rf ./lib
	rm -f ./test/**/*.js ./test/*.js

.PHONY: build build-test all dev-install loc clean test benchmark

test: build build-test
	$(MOCHA) --check-leaks -R dot ./test
