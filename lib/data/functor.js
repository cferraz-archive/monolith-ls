var adt, Functor;
adt = require('../core/adt');
module.exports = adt(Functor = (function(){
  Functor.displayName = 'Functor';
  var prototype = Functor.prototype, constructor = Functor;
  prototype.map = adt.needed;
  function Functor(){}
  return Functor;
}()));
//# sourceMappingURL=functor.js.map
