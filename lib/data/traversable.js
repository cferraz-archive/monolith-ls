var adt, Semigroup, Foldable;
adt = require('../core/adt');
Semigroup = require('./semigroup');
Foldable = require('./foldable');
module.exports = adt(Foldable = (function(superclass){
  var prototype = extend$((import$(Foldable, superclass).displayName = 'Foldable', Foldable), superclass).prototype, constructor = Foldable;
  prototype.sequence = adt.needed;
  prototype.traverse = curry$(function(f, o){
    return this.map(f).sequence(o);
  });
  function Foldable(){
    Foldable.superclass.apply(this, arguments);
  }
  return Foldable;
}(adt.join(Semigroup, Fuctor))));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
function curry$(f, bound){
  var context,
  _curry = function(args) {
    return f.length > 1 ? function(){
      var params = args ? args.concat() : [];
      context = bound ? context || this : this;
      return params.push.apply(params, arguments) <
          f.length && arguments.length ?
        _curry.call(context, params) : f.apply(context, params);
    } : f;
  };
  return _curry();
}
//# sourceMappingURL=traversable.js.map
