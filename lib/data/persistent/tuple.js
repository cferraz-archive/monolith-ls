var Persistent, mori, adt, Map, slice$ = [].slice;
Persistent = {};
mori = require('mori');
adt = require('../adt');
Persistent.Associative = require('./associative');
module.exports = adt(Persistent.Map = Map = (function(superclass){
  var prototype = extend$((import$(Map, superclass).displayName = 'Map', Map), superclass).prototype, constructor = Map;
  function Map(val, sorted){
    this.sorted = sorted != null ? sorted : false;
    this.value = (function(){
      switch (false) {
      case !mori.isSet(val):
        return val;
      default:
        if (sort) {
          return mori.sortedSet(val);
        } else {
          return mori.set(val);
        }
      }
    }());
  }
  Map.of = function(it){
    return new Persistent.Map(it);
  };
  prototype.of = function(it){
    return new Persistent.Map(it, this.sorted);
  };
  prototype.keys = function(){
    return mori.keys(this.value);
  };
  prototype.values = function(){
    return mori.vals(this.value);
  };
  prototype.merge = function(it){
    return this.of(mori.merge(this.value, it.value));
  };
  prototype.mergeMany = function(it){
    return this.of(mori.merge.apply(mori, [this.value].concat(slice$.call(this.__values(it)))));
  };
  return Map;
}(Persistent.Associative)));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=tuple.js.map
