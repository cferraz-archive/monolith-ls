var Persistent, mori, adt, Associative, slice$ = [].slice;
Persistent = {};
mori = require('mori');
adt = require('../adt');
Persistent.Collection = require('./collection');
module.exports = adt(Persistent.Associative = Associative = (function(superclass){
  var prototype = extend$((import$(Associative, superclass).displayName = 'Associative', Associative), superclass).prototype, constructor = Associative;
  function Associative(){
    throw Error('unimplemented');
  }
  prototype.assoc = function(it){
    return this.of(mori.assoc(this.value, it));
  };
  prototype.assocMany = function(it){
    return this.of(mori.assoc.apply(mori, [this.value].concat(slice$.call(it))));
  };
  prototype.dissoc = function(it){
    return this.of(mori.dissoc(this.value, it));
  };
  prototype.dissocMany = function(it){
    return this.of(mori.dissoc.apply(mori, [this.value].concat(slice$.call(it))));
  };
  return Associative;
}(Persistent.Collection)));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=associative.js.map
