var Persistent, mori, adt, List;
Persistent = {};
mori = require('mori');
adt = require('../adt');
Persistent.Sequence = require('./sequence');
module.exports = adt(Persistent.List = List = (function(superclass){
  var prototype = extend$((import$(List, superclass).displayName = 'List', List), superclass).prototype, constructor = List;
  function List(it){
    this.value = mori.isList(it)
      ? it
      : mori.list(it);
  }
  List.of = function(it){
    return new Persistent.List(it);
  };
  prototype.of = function(it){
    return Persistent.List.of(it);
  };
  prototype.peek = function(){
    return mori.peek(this.value);
  };
  prototype.pop = function(){
    return this.of(mori.pop(this.value));
  };
  return List;
}(Persistent.Sequence)));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=list.js.map
