var Persistent, mori, adt, Foldable, Monoid, Functor, SequenceCollection, Sequence, slice$ = [].slice;
Persistent = {};
mori = require('mori');
adt = require('../adt');
Foldable = require('../foldable');
Monoid = require('../monoid');
Functor = require('../../control/functor');
Persistent.Map = require('./map');
Persistent.Collection = require('./collection');
SequenceCollection = adt.join(Monoid, Foldable, Functor, Persistent.Collection);
module.exports = adt()(Persistent.Sequence = Sequence = (function(superclass){
  var prototype = extend$((import$(Sequence, superclass).displayName = 'Sequence', Sequence), superclass).prototype, constructor = Sequence;
  function Sequence(){
    throw Error('unimplemented');
  }
  Sequence.cons = curry$(function(v, c){
    return this.of(mori.cons(c, this.value));
  });
  Sequence.toSequence = function(it){
    return this.of(mori.seq(it));
  };
  prototype.toArray = function(){
    return mori.intoArray(this.value);
  };
  prototype.distinct = function(){
    return this.of(mori.distinct(this.value));
  };
  prototype.first = function(){
    return mori.first(this.valeu);
  };
  prototype.last = function(){
    return mori.last(this.value);
  };
  prototype.rest = function(){
    return this.of(mori.rest(this.value));
  };
  prototype.reverse = function(){
    return this.of(mori.reverse(this.value));
  };
  prototype.flatten = function(){
    return this.of(mori.flatten(this.value));
  };
  prototype.concat = function(it){
    return this.of(mori.concat(this.value, it.value));
  };
  prototype.concatMany = function(it){
    return this.of(mori.concat.apply(mori, [this.value].concat(slice$.call(this.__values(it)))));
  };
  prototype.map = function(f){
    return this.of(mori.map(f, this.value));
  };
  prototype.mapMany = function(f, a){
    return this.of(mori.map.apply(mori, [f, this.value].concat(slice$.call(this.__values(it)))));
  };
  prototype.concatMap = function(f){
    return this.of(mori.mapcat(f, this.value));
  };
  prototype.concatMapMany = curry$(function(f, a){
    return mori.mapcat.apply(mori, [f, this.value].concat(slice$.call(this.__values(it))));
  });
  prototype.filter = function(f){
    return this.of(mori.filter(f, this.value));
  };
  prototype.remove = function(f){
    return this.of(mori.remove(f, this.value));
  };
  prototype.reduce = curry$(function(i, f){
    return this.of(mori.reduce(f, i, this.value));
  });
  prototype.reduce1 = function(f){
    return this.of(mori.reduce(f, this.value));
  };
  prototype.reduceTuple = curry$(function(i, f){
    return this.of(mori.reduceKV(f, i, this.value));
  });
  prototype.reduce1Tuple = function(f){
    return this.of(mori.reduceKV(f, this.value));
  };
  prototype.take = function(it){
    return this.of(mori.take(it, this.value));
  };
  prototype.takeWhile = function(f){
    return this.of(mori.takeWhile(f, this.value));
  };
  prototype.drop = function(it){
    return this.of(mori.drop(it, this.value));
  };
  prototype.dropWhile = function(f){
    return this.of(mori.dropWhile(f, this.value));
  };
  prototype.some = function(f){
    return this.of(mori.some(f, this.value));
  };
  prototype.every = function(f){
    return this.of(mori.every(f, this.value));
  };
  prototype.sort = function(){
    return this.of(mori.sort(this.value));
  };
  prototype.sortWith = function(f){
    return this.of(mori.sort(f, this.value));
  };
  prototype.sortBy = function(it){
    return this.of(mori.sortBy(it, this.value));
  };
  prototype.sortByWith = curry$(function(k, f){
    return this.of(mori.sortBy(k, f, this.value));
  });
  prototype.interpose = function(it){
    return this.of(mori.interpose(it, this.value));
  };
  prototype.interleave = function(it){
    return this.of(mori.interleave(this.value, it.value));
  };
  prototype.interleaveMany = function(it){
    return this.of(mori.interleave.apply(mori, [this.value].concat(slice$.call(this.__values(it)))));
  };
  prototype.iterate = function(f){
    return this.of(mori.iterate(f, this.value));
  };
  prototype.repeat = function(it){
    switch (it != null) {
    case true:
      return this.of(mori.repeat(it, this.value));
    default:
      return this.of(mori.repeat(this.value));
    }
  };
  prototype.repeatedly = function(n){
    switch (typeof it != 'undefined' && it !== null) {
    case true:
      return this.of(mori.repeatedly(it, this.value));
    default:
      return this.of(mori.repeatedly(this.value));
    }
  };
  prototype.partition = function(n){
    return this.of(mori.partition(n, this.value));
  };
  prototype.detailedPartition = curry$(function(n, s, p){
    return this.of(mori.partition(n, s, p, this.value));
  });
  prototype.partitionBy = function(f){
    return this.of(mori.partitionBy(f, this.value));
  };
  prototype.groupBy = function(f){
    return this.of(mori.groupBy(f, this.value));
  };
  prototype.zipmap = function(it){
    return Persistent.Map.of(mori.zipmap(this.value, it.value));
  };
  return Sequence;
}(SequenceCollection)));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
function curry$(f, bound){
  var context,
  _curry = function(args) {
    return f.length > 1 ? function(){
      var params = args ? args.concat() : [];
      context = bound ? context || this : this;
      return params.push.apply(params, arguments) <
          f.length && arguments.length ?
        _curry.call(context, params) : f.apply(context, params);
    } : f;
  };
  return _curry();
}
//# sourceMappingURL=sequence.js.map
