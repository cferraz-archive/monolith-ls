var Persistent, mori, adt, AssociativeList, Vector;
Persistent = {};
mori = require('mori');
adt = require('../adt');
Persistent.Associative = require('./associative');
Persistent.List = require('./list');
AssociativeList = adt.join(Persistent.Associative, Persistent.List);
module.exports = adt(Persistent.Vector = Vector = (function(superclass){
  var prototype = extend$((import$(Vector, superclass).displayName = 'Vector', Vector), superclass).prototype, constructor = Vector;
  function Vector(it){
    this.value = mori.isVector(it)
      ? it
      : mori.vector(it);
  }
  Vector.of = function(it){
    return new Persistent.Vector(it);
  };
  prototype.of = function(it){
    return Persistent.Vector.of(it);
  };
  prototype.subvec = curry$(function(a, b){
    return mori.subvec(this.value, a, b);
  });
  prototype.slice = curry$(function(a, b){
    return mori.subvec(this.value, a, a + b);
  });
  prototype.from = function(it){
    return mori.subvec(this.value, it);
  };
  prototype.to = function(it){
    return mori.subvec(this.value, 0, it);
  };
  return Vector;
}(AssociativeList)));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
function curry$(f, bound){
  var context,
  _curry = function(args) {
    return f.length > 1 ? function(){
      var params = args ? args.concat() : [];
      context = bound ? context || this : this;
      return params.push.apply(params, arguments) <
          f.length && arguments.length ?
        _curry.call(context, params) : f.apply(context, params);
    } : f;
  };
  return _curry();
}
//# sourceMappingURL=vector.js.map
