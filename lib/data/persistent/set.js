var Persistent, mori, adt, Set, slice$ = [].slice;
Persistent = {};
mori = require('mori');
adt = require('../adt');
Persistent.List = require('./list');
module.exports = adt(Persistent.Set = Set = (function(superclass){
  var prototype = extend$((import$(Set, superclass).displayName = 'Set', Set), superclass).prototype, constructor = Set;
  function Set(val, sorted){
    this.sorted = sorted != null ? sorted : false;
    this.value = (function(){
      switch (false) {
      case !mori.isSet(val):
        return val;
      default:
        if (sort) {
          return mori.sortedSet(val);
        } else {
          return mori.set(val);
        }
      }
    }());
  }
  Set.of = function(it){
    return new Persistent.Set(it);
  };
  prototype.of = function(it){
    return new Persistent.Set(it, this.sorted);
  };
  prototype.disj = function(it){
    return this.of(mori.disj(this.value, it));
  };
  prototype.union = function(it){
    return this.of(mori.union(this.value, it.value));
  };
  prototype.unionMany = function(it){
    return this.of(mori.union.apply(mori, [this.value].concat(slice$.call(this.__values(it)))));
  };
  prototype.intersection = function(it){
    return this.of(mori.intersection(this.value, it.value));
  };
  prototype.intersectionMany = function(it){
    return this.of(mori.intersection.apply(mori, [this.value].concat(slice$.call(this.__values(it)))));
  };
  prototype.difference = function(it){
    return this.of(mori.difference(this.value, it.value));
  };
  prototype.differenceMany = function(it){
    return this.of(mori.difference.apply(mori, [this.value].concat(slice$.call(this.__values(it)))));
  };
  prototype.isSubset = function(it){
    return mori.isSubset(this.value, it.value);
  };
  prototype.isSuperset = function(it){
    return mori.isSuperset(this.value, it.value);
  };
  return Set;
}(Persistent.List)));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=set.js.map
