var Persistent, mori, adt, Queue;
Persistent = {};
mori = require('mori');
adt = require('../adt');
Persistent.Set = require('./set');
module.exports = adt(Persistent.Queue = Queue = (function(superclass){
  var prototype = extend$((import$(Queue, superclass).displayName = 'Queue', Queue), superclass).prototype, constructor = Queue;
  function Queue(it){
    this.value = mori.queue(it);
  }
  Queue.of = function(it){
    return new Persistent.Queue(it);
  };
  prototype.of = function(it){
    return Persistent.Queue.of(it);
  };
  return Queue;
}(Persistent.Set)));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=queue.js.map
