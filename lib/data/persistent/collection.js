var mori, map, adt, Collection, slice$ = [].slice;
mori = require('mori');
map = require('../list/map');
adt = require('../adt');
module.exports = adt(Collection = (function(){
  Collection.displayName = 'Collection';
  var prototype = Collection.prototype, constructor = Collection;
  function Collection(){
    throw Error('unimplemented');
  }
  prototype.__values = function(it){
    return map(function(it){
      return it.value;
    })(
    it);
  };
  prototype.equals = function(it){
    return mori.equals(this.value, it);
  };
  prototype.hash = function(){
    return mori.hash(this.value);
  };
  prototype.conj = function(it){
    return mori.conj(this.value, it);
  };
  prototype.conjMany = function(it){
    return mori.conj.apply(mori, [this.value].concat(slice$.call(it)));
  };
  prototype.into = function(it){
    return mori.into(this.value, it);
  };
  prototype.get = function(it){
    return mori.get(this.value, it);
  };
  prototype.getIn = function(it){
    return mori.getIn(this.value, it);
  };
  prototype.hasKey = function(it){
    return mori.hasKey(this.value, it);
  };
  prototype.find = function(it){
    return mori.find(this.value, it);
  };
  prototype.nth = function(it){
    return mori.nth(this.value, it);
  };
  prototype.assocIn = curry$(function(v, k){
    return mori.assocIn(this.value, k, v);
  });
  prototype.updateIn = curry$(function(k, f){
    return mori.updateIn(this.value, k(f));
  });
  prototype.count = function(){
    return mori.count(this.value);
  };
  prototype.empty = function(){
    return this.of(mori.empty(this.value));
  };
  prototype.isEmpty = function(){
    return mori.isEmpty(this.value);
  };
  prototype.native = function(){
    return mori.toJs(this.value);
  };
  Collection.isList = function(it){
    return mori.isList(it);
  };
  Collection.isSeq = function(it){
    return mori.isSeq(it);
  };
  Collection.isVector = function(it){
    return mori.isVector(it);
  };
  Collection.isMap = function(it){
    return mori.isMap(it);
  };
  Collection.isSet = function(it){
    return mori.isSet(it);
  };
  Collection.isCollection = function(it){
    return mori.isCollection(it);
  };
  Collection.isSequential = function(it){
    return mori.isSequential(it);
  };
  Collection.isAssociative = function(it){
    return mori.isAssociative(it);
  };
  Collection.isCounted = function(it){
    return mori.isCounted(it);
  };
  Collection.isIndexed = function(it){
    return mori.isIndexed(it);
  };
  Collection.isReduceable = function(it){
    return mori.isReduceable(it);
  };
  Collection.isSeqable = function(it){
    return mori.isSeqable(it);
  };
  Collection.isReversible = function(it){
    return mori.isReversible(it);
  };
  return Collection;
}()));
function curry$(f, bound){
  var context,
  _curry = function(args) {
    return f.length > 1 ? function(){
      var params = args ? args.concat() : [];
      context = bound ? context || this : this;
      return params.push.apply(params, arguments) <
          f.length && arguments.length ?
        _curry.call(context, params) : f.apply(context, params);
    } : f;
  };
  return _curry();
}
//# sourceMappingURL=collection.js.map
