var Persistent, mori, adt, Range;
Persistent = {};
mori = require('mori');
adt = require('../adt');
Persistent.Sequence = require('./sequence');
module.exports = adt(Persistent.Range = Range = (function(superclass){
  var prototype = extend$((import$(Range, superclass).displayName = 'Range', Range), superclass).prototype, constructor = Range;
  function Range(it){
    this.value = mori.range(it);
  }
  Range.of = function(it){
    return new Persistent.Range(it);
  };
  prototype.of = function(it){
    return Persistent.Range.of(it);
  };
  return Range;
}(Persistent.Sequence)));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=range.js.map
