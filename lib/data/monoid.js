var adt, Semigroup, Monoid;
adt = require('../core/adt');
Semigroup = require('./semigroup');
module.exports = adt(Monoid = (function(superclass){
  var prototype = extend$((import$(Monoid, superclass).displayName = 'Monoid', Monoid), superclass).prototype, constructor = Monoid;
  prototype.empty = adt.needed;
  function Monoid(){
    Monoid.superclass.apply(this, arguments);
  }
  return Monoid;
}(Semigroup)));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=monoid.js.map
