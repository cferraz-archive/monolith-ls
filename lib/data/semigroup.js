var adt, Semigroup;
adt = require('../core/adt');
module.exports = adt(Semigroup = (function(){
  Semigroup.displayName = 'Semigroup';
  var prototype = Semigroup.prototype, constructor = Semigroup;
  prototype.concat = adt.needed;
  function Semigroup(){}
  return Semigroup;
}()));
//# sourceMappingURL=semigroup.js.map
