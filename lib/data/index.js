module.exports = {
  Functor: require('./functor'),
  Setoid: require('./setoid'),
  Semigroup: require('./semigroup'),
  Foldable: require('./foldable'),
  Monoid: require('./monoid'),
  Traversable: require('./traversable'),
  Persistent: require('./Persistent')
};
//# sourceMappingURL=index.js.map
