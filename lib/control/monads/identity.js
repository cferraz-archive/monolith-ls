var Monads, adt, Monad, Identity;
Monads = {};
adt = require('../../core/adt');
Monad = require('../monad');
module.exports = adt()(Monads.Identity = Identity = (function(superclass){
  var prototype = extend$((import$(Identity, superclass).displayName = 'Identity', Identity), superclass).prototype, constructor = Identity;
  function Identity(value){
    this.value = value;
  }
  prototype.of = function(it){
    return new Identity(it);
  };
  prototype.chain = function(f){
    var v;
    if ((v = f(this.value)).of != null) {
      return v;
    } else {
      return this.of(v);
    }
  };
  return Identity;
}(Monad)));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=identity.js.map
