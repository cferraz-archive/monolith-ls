var Monads, adt, Monad, Nothing;
Monads = {};
adt = require('../../core/adt');
Monad = require('../monad');
module.exports = adt()(Monads.Nothing = Nothing = (function(superclass){
  var prototype = extend$((import$(Nothing, superclass).displayName = 'Nothing', Nothing), superclass).prototype, constructor = Nothing;
  function Nothing(){}
  prototype.of = function(){
    return Nothing;
  };
  prototype.isJust = function(){
    return false;
  };
  prototype.isNothing = function(){
    return true;
  };
  prototype.chain = function(f){
    return this;
  };
  return Nothing;
}(Monad)));
Nothing = new Monads.Nothing();
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=nothing.js.map
