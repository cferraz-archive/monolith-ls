var Monads, adt, Monad, Just;
Monads = {};
adt = require('../../core/adt');
Monad = require('../monad');
module.exports = adt()(Monads.Just = Just = (function(superclass){
  var prototype = extend$((import$(Just, superclass).displayName = 'Just', Just), superclass).prototype, constructor = Just;
  function Just(value){
    this.value = value;
  }
  prototype.of = function(it){
    return new Just(it);
  };
  prototype.isJust = function(){
    return true;
  };
  prototype.isNothing = function(){
    return false;
  };
  prototype.chain = function(f){
    var v;
    if ((v = f(this.value)).of != null) {
      return v;
    } else {
      return this.of(v);
    }
  };
  return Just;
}(Monad)));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=just.js.map
