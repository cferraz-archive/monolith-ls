var adt, Applicative, Chain, Monad;
adt = require('../core/adt');
Applicative = require('./applicative');
Chain = require('./chain');
module.exports = adt(Monad = (function(superclass){
  var prototype = extend$((import$(Monad, superclass).displayName = 'Monad', Monad), superclass).prototype, constructor = Monad;
  prototype.ap = function(m){
    return this.chain(function(f){
      return m.map(f);
    });
  };
  prototype.map = function(f){
    var m;
    m = this;
    return m.chain(function(a){
      return m.of(f(a));
    });
  };
  function Monad(){
    Monad.superclass.apply(this, arguments);
  }
  return Monad;
}(adt.join(Applicative, Chain))));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=monad.js.map
