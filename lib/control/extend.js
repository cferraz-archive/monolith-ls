var adt, Extend;
adt = require('../core/adt');
module.exports = adt(Extend = (function(){
  Extend.displayName = 'Extend';
  var prototype = Extend.prototype, constructor = Extend;
  prototype.extend = adt.needed;
  function Extend(){}
  return Extend;
}()));
//# sourceMappingURL=extend.js.map
