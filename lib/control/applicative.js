var adt, Apply, Functor, Applicative;
adt = require('../core/adt');
Apply = require('./apply');
Functor = require('./functor');
adt(Applicative = (function(superclass){
  var prototype = extend$((import$(Applicative, superclass).displayName = 'Applicative', Applicative), superclass).prototype, constructor = Applicative;
  prototype.of = adt.needed;
  prototype.map = function(f){
    return this.of(f).ap(this);
  };
  function Applicative(){
    Applicative.superclass.apply(this, arguments);
  }
  return Applicative;
}(adt.join(Functor, Apply))));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=applicative.js.map
