var adt, Functor, Extend, Comonad;
adt = require('../core/adt');
Functor = require('../functor');
Extend = require('../extend');
module.exports = adt(Comonad = (function(superclass){
  var prototype = extend$((import$(Comonad, superclass).displayName = 'Comonad', Comonad), superclass).prototype, constructor = Comonad;
  prototype.extract = adt.needed;
  function Comonad(){
    Comonad.superclass.apply(this, arguments);
  }
  return Comonad;
}(adt.join(Functor, Extend))));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=comonad.js.map
