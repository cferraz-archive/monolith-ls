var adt, Apply, Chain;
adt = require('../core/adt');
Apply = require('./apply');
module.exports = adt(Chain = (function(superclass){
  var prototype = extend$((import$(Chain, superclass).displayName = 'Chain', Chain), superclass).prototype, constructor = Chain;
  prototype.chain = adt.needed;
  prototype.ap = function(m){
    return this.chain(function(f){
      if (typeof f === 'function') {
        return m.map(f);
      }
    });
  };
  prototype.map = function(f){
    var this$ = this;
    return this.chain(function(a){
      return this$.of(f(a));
    });
  };
  function Chain(){
    Chain.superclass.apply(this, arguments);
  }
  return Chain;
}(Apply)));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=chain.js.map
