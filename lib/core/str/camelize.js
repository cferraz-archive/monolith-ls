var this$ = this;
module.exports = function(it){
  return it.replace(/[-_]+(.)?/g, function(arg$, c){
    return (c != null ? c : '').toUpperCase();
  });
};
//# sourceMappingURL=camelize.js.map
