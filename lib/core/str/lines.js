module.exports = function(str){
  if (!str.length) {
    return [];
  }
  return str.split('\n');
};
//# sourceMappingURL=lines.js.map
