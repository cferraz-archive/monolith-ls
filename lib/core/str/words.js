module.exports = function(str){
  if (!str.length) {
    return [];
  }
  return str.split(/[ ]+/);
};
//# sourceMappingURL=words.js.map
