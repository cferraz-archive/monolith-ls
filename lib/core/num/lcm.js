var gcd;
gcd = require('./gcd');
module.exports = curry$(function(x, y){
  return Math.abs(Math.floor(x / gcd(x, y) * y));
});
function curry$(f, bound){
  var context,
  _curry = function(args) {
    return f.length > 1 ? function(){
      var params = args ? args.concat() : [];
      context = bound ? context || this : this;
      return params.push.apply(params, arguments) <
          f.length && arguments.length ?
        _curry.call(context, params) : f.apply(context, params);
    } : f;
  };
  return _curry();
}
//# sourceMappingURL=lcm.js.map
