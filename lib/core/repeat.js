var repeat, slice$ = [].slice, toString$ = {}.toString;
repeat = function(fn){
  return function(){
    var initArgs, stop, args, output, recur;
    initArgs = slice$.call(arguments);
    stop = false;
    args = [].concat(initArgs);
    recur = function(it){
      output = it;
      return stop = false;
    };
    recur.stop = function(it){
      output = it;
      return stop = true;
    };
    while (stop === false) {
      stop = true;
      fn.apply(null, slice$.call(args).concat([recur]));
      if (stop === false && output != null) {
        if (toString$.call(output).slice(8, -1) === 'Array') {
          args = output;
        } else {
          args = [output];
        }
      }
    }
    return output;
  };
};
module.exports = repeat;
//# sourceMappingURL=repeat.js.map
