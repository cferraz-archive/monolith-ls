var unique, each, map, flatten, curry, filter, foldl, forbid, adt, adtValidate, dependenceTree, slice$ = [].slice;
unique = require('./list/unique');
each = require('./list/each');
map = require('./list/map');
flatten = require('./list/flatten');
curry = require('./func/curry');
filter = require('./list/filter');
foldl = require('./list/foldl');
forbid = ['__adt_keys', '__adt_proto_keys', '__type_join', 'displayName', 'superclass', 'constructor', 'isA'];
module.exports = adt = function(klass){
  var adtK, adtPK;
  if (klass == null) {
    return adtValidate;
  }
  adtK = klass.__adt_keys || [];
  adtK = unique(adtK.concat(Object.keys(klass)));
  adtK = filter(function(it){
    return forbid.indexOf(it) === -1;
  })(
  adtK);
  klass.__adt_keys = adtK;
  adtPK = klass.__adt_proto_keys || [];
  adtPK = unique(adtPK.concat(Object.keys(klass.prototype)));
  adtPK = filter(function(it){
    return forbid.indexOf(it) === -1;
  })(
  adtPK);
  klass.__adt_proto_keys = adtPK;
  each(function(k){
    if (klass[k] === adt.needed) {
      return klass[k] = klass[k](klass.displayName, k);
    }
  })(
  klass.__adt_keys);
  each(function(k){
    if (klass.prototype[k] === adt.needed) {
      return klass.prototype[k] = klass.prototype[k](klass.displayName, "::" + k);
    } else if (!klass.prototype[k].__adt_needed) {
      return klass[k] = curry(klass.prototype[k].lenght, function(){
        var args, ref$;
        args = slice$.call(arguments);
        return (ref$ = klass.prototype)[k].apply(ref$, args);
      });
    }
  })(
  klass.__adt_proto_keys);
  klass.is = function(t){
    var ref$, ref1$;
    switch (false) {
    case klass !== t:
      return true;
    case klass.constructor !== t:
      return true;
    case -1 === ((ref$ = klass.__type_join) != null ? ref$.indexOf(t) : void 8):
      return true;
    case ((ref1$ = klass.superclass) != null ? ref1$.is : void 8) == null:
      return klass.superclass.is(t);
    default:
      return false;
    }
  };
  klass.prototype.is = function(t){
    return this.constructor.isOf(t);
  };
  return klass;
};
adtValidate = function(klass){
  klass = adt(klass);
  /*
  klass.__adt_keys |> each (k) ->
    if klass[k].__adt_needed? and klass[k].__adt_needed
    then klass[k]!
  
  klass.__adt_proto_keys |> each (k) ->
    if klass::[k].__adt_needed? and klass::[k].__adt_needed
    then klass::[k]!
  */
  return klass;
};
adt.needed = function(ʹfrom, name){
  var f;
  f = function(){
    throw TypeError(ʹfrom + " needs function " + name);
  };
  f.__adt_needed = true;
  return f;
};
dependenceTree = function(ʹklass, acc){
  var ʹacc;
  acc == null && (acc = []);
  if (ʹklass.__type_join != null) {
    ʹacc = map(function(ʺklass){
      var ʺacc;
      ʺacc = recursiveWalk(ʺklass, [acc, ʺklass]);
      return [acc, ʺklass, ʺacc];
    })(
    ʹklass.__type_join);
    return [acc, ʹacc, ʹklass];
  } else {
    return [acc, ʹklass];
  }
};
adt.join = function(){
  var classes, klass, keys, foldClasses;
  classes = slice$.call(arguments);
  klass = function(){};
  klass.__adt_keys = [];
  klass.__adt_proto_keys = [];
  klass.__type_join = flatten(map(function(it){
    return dependenceTree(it);
  })(
  classes));
  keys = function(it){
    return Object.keys(it);
  };
  foldClasses = foldl(function(c, n){
    var ʹk, ʺk;
    ʹk = unique(keys(n).concat(n.__adt_keys || []));
    ʺk = unique(keys(n).concat(n.__adt_proto_keys || []));
    each(function(k){
      if (-1 !== forbid.indexOf(k)) {
        if (c[k] == null || (c[k] != null && c[k].__adt_needed)) {
          return c[k] = n[k];
        }
      }
    })(
    ʹk);
    each(function(k){
      if (-1 !== forbid.indexOf(k)) {
        if (c[k] == null || (c[k] != null && c[k].__adt_needed)) {
          return c[k] = n[k];
        }
      }
    })(
    ʺk);
    return c;
  });
  return foldClasses(klass, classes);
};
//# sourceMappingURL=adt.js.map
