var α;
module.exports = α = (function(){
  switch (false) {
  case typeof process == 'undefined' || process === null:
    return process.nextTick;
  case typeof setImediate == 'undefined' || setImediate === null:
    return setImediate;
  default:
    return function(it){
      return setTimeout(it, Infinity);
    };
  }
}());
//# sourceMappingURL=async.js.map
