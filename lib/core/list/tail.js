module.exports = function(xs){
  if (!xs.length) {
    return;
  }
  return xs.slice(1);
};
//# sourceMappingURL=tail.js.map
