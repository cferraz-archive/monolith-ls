var takeWhile, dropWhile;
takeWhile = require('./take-while');
dropWhile = require('./drop-while');
module.exports = curry$(function(p, xs){
  return [takeWhile(p, xs), dropWhile(p, xs)];
});
function curry$(f, bound){
  var context,
  _curry = function(args) {
    return f.length > 1 ? function(){
      var params = args ? args.concat() : [];
      context = bound ? context || this : this;
      return params.push.apply(params, arguments) <
          f.length && arguments.length ?
        _curry.call(context, params) : f.apply(context, params);
    } : f;
  };
  return _curry();
}
//# sourceMappingURL=span.js.map
