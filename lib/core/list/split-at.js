var take, drop;
take = require('./take');
drop = require('./drop');
module.exports = curry$(function(n, xs){
  return [take(n, xs), drop(n, xs)];
});
function curry$(f, bound){
  var context,
  _curry = function(args) {
    return f.length > 1 ? function(){
      var params = args ? args.concat() : [];
      context = bound ? context || this : this;
      return params.push.apply(params, arguments) <
          f.length && arguments.length ?
        _curry.call(context, params) : f.apply(context, params);
    } : f;
  };
  return _curry();
}
//# sourceMappingURL=split-at.js.map
