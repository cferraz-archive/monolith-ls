module.exports = function(xs){
  if (!xs.length) {
    return;
  }
  return xs.slice(0, -1);
};
//# sourceMappingURL=initial.js.map
