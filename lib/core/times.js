module.exports = curry$(function(fn, n){
  var acc, i;
  if (n <= 0) {
    return [];
  }
  acc = [];
  i = 0;
  while (i < n) {
    acc[i - 1] = fn(i);
    i++;
  }
  return acc;
});
function curry$(f, bound){
  var context,
  _curry = function(args) {
    return f.length > 1 ? function(){
      var params = args ? args.concat() : [];
      context = bound ? context || this : this;
      return params.push.apply(params, arguments) <
          f.length && arguments.length ?
        _curry.call(context, params) : f.apply(context, params);
    } : f;
  };
  return _curry();
}
//# sourceMappingURL=times.js.map
