var curry2, curry3, curry4, curry5, curry6, curry7, curry8, curry9;
module.exports = function(bound_, length_, fx_){
  var fx, length, bound, fn;
  switch (false) {
  case fx_ == null:
    fx = fx_;
    length = length_;
    bound = bound_;
    break;
  case length_ == null:
    if ('number' === typeof bound_) {
      fx = length_;
      length = bound_;
      bound = void 8;
    } else {
      fx = length_;
      length = fx.length;
      bound = bound_;
    }
    break;
  case bound_ == null:
    fx = bound_;
    length = fx.length;
    bound = void 8;
  }
  fn = bound != null ? bound === true
    ? fx.bind(this)
    : !!bound ? fx.bind(bound) : fx : fx;
  if (length > 9) {
    throw new Error("Trying to curry too many arguments (max: 9).");
  } else {
    switch (length) {
    case 0:
      return fn;
    case 1:
      return fn;
    case 2:
      return curry2(fn, length);
    case 3:
      return curry3(fn, length);
    case 4:
      return curry4(fn, length);
    case 5:
      return curry5(fn, length);
    case 6:
      return curry6(fn, length);
    case 7:
      return curry7(fn, length);
    case 8:
      return curry8(fn, length);
    case 9:
      return curry9(fn, length);
    }
  }
};
curry2 = function(fn, length){
  return function(z0, z1){
    switch (false) {
    case arguments.length !== 2:
      return fn(z0, z1);
    default:
      return function(a){
        return fn(z0, a);
      };
    }
  };
};
curry3 = function(fn, length){
  return function(z0, z1, z2){
    switch (false) {
    case arguments.length !== 3:
      return fn(z0, z1, z2);
    default:
      switch (length - arguments.length) {
      case 1:
        return function(a){
          return fn(z0, z1, a);
        };
      case 2:
        return curry2(function(a, b){
          return fn(z0, a, b);
        });
      }
    }
  };
};
curry4 = function(fn, length){
  return function(z0, z1, z2, z3){
    switch (false) {
    case arguments.length !== 4:
      return fn(z0, z1, z2, z3);
    default:
      switch (length - arguments.length) {
      case 1:
        return function(a){
          return fn(z0, z1, z2, a);
        };
      case 2:
        return curry2(function(a, b){
          return fn(z0, z1, a, b);
        });
      case 3:
        return curry3(function(a, b, c){
          return fn(z0, a, b, c);
        });
      }
    }
  };
};
curry5 = function(fn, length){
  return function(z0, z1, z2, z3, z4){
    switch (false) {
    case arguments.length !== 5:
      return fn(z0, z1, z2, z3, z4);
    default:
      switch (length - arguments.length) {
      case 1:
        return function(a){
          return fn(z0, z1, z2, z3, a);
        };
      case 2:
        return curry2(function(a, b){
          return fn(z0, z1, z2, a, b);
        });
      case 3:
        return curry3(function(a, b, c){
          return fn(z0, z1, a, b, c);
        });
      case 4:
        return curry4(function(a, b, c, d){
          return fn(z0, a, b, c, d);
        });
      }
    }
  };
};
curry6 = function(fn, length){
  return function(z0, z1, z2, z3, z4, z5){
    switch (false) {
    case arguments.length !== 6:
      return fn(z0, z1, z2, z3, z4, z5);
    default:
      switch (length - arguments.length) {
      case 1:
        return function(a){
          return fn(z0, z1, z2, z3, z4, a);
        };
      case 2:
        return curry2(function(a, b){
          return fn(z0, z1, z2, z3, a, b);
        });
      case 3:
        return curry3(function(a, b, c){
          return fn(z0, z1, z2, a, b, c);
        });
      case 4:
        return curry4(function(a, b, c, d){
          return fn(z0, z1, a, b, c, d);
        });
      case 5:
        return curry5(function(a, b, c, d, e){
          return fn(z0, a, b, c, d, e);
        });
      }
    }
  };
};
curry7 = function(fn, length){
  return function(z0, z1, z2, z3, z4, z5, z6){
    switch (false) {
    case arguments.length !== 7:
      return fn(z0, z1, z2, z3, z4, z5, z6);
    default:
      switch (length - arguments.length) {
      case 1:
        return function(a){
          return fn(z0, z1, z2, z3, z4, z5, a);
        };
      case 2:
        return curry2(function(a, b){
          return fn(z0, z1, z2, z3, z4, a, b);
        });
      case 3:
        return curry3(function(a, b, c){
          return fn(z0, z1, z2, z3, a, b, c);
        });
      case 4:
        return curry4(function(a, b, c, d){
          return fn(z0, z1, z2, a, b, c, d);
        });
      case 5:
        return curry5(function(a, b, c, d, e){
          return fn(z0, z1, a, b, c, d, e);
        });
      case 6:
        return curry6(function(a, b, c, d, e, f){
          return fn(z0, a, b, c, d, e, f);
        });
      }
    }
  };
};
curry8 = function(fn, length){
  return function(z0, z1, z2, z3, z4, z5, z6, z7){
    switch (false) {
    case arguments.length !== 8:
      return fn(z0, z1, z2, z3, z4, z5, z6, z7);
    default:
      switch (length - arguments.length) {
      case 1:
        return function(a){
          return fn(z0, z1, z2, z3, z4, z5, z6, a);
        };
      case 2:
        return curry2(function(a, b){
          return fn(z0, z1, z2, z3, z4, z5, a, b);
        });
      case 3:
        return curry3(function(a, b, c){
          return fn(z0, z1, z2, z3, z4, a, b, c);
        });
      case 4:
        return curry4(function(a, b, c, d){
          return fn(z0, z1, z2, z3, a, b, c, d);
        });
      case 5:
        return curry5(function(a, b, c, d, e){
          return fn(z0, z1, z2, a, b, c, d, e);
        });
      case 6:
        return curry6(function(a, b, c, d, e, f){
          return fn(z0, z1, a, b, c, d, e, f);
        });
      case 7:
        return curry7(function(a, b, c, d, e, f, g){
          return fn(z0, a, b, c, d, e, f, g);
        });
      }
    }
  };
};
curry9 = function(fn, length){
  return function(z0, z1, z2, z3, z4, z5, z6, z7, z8){
    switch (false) {
    case arguments.length !== 9:
      return fn(z0, z1, z2, z3, z4, z5, z6, z7, z8);
    default:
      switch (length - arguments.length) {
      case 1:
        return function(a){
          return fn(z0, z1, z2, z3, z4, z5, z6, z7, a);
        };
      case 2:
        return curry2(function(a, b){
          return fn(z0, z1, z2, z3, z4, z5, z6, a, b);
        });
      case 3:
        return curry3(function(a, b, c){
          return fn(z0, z1, z2, z3, z4, z5, a, b, c);
        });
      case 4:
        return curry4(function(a, b, c, d){
          return fn(z0, z1, z2, z3, z4, a, b, c, d);
        });
      case 5:
        return curry5(function(a, b, c, d, e){
          return fn(z0, z1, z2, z3, a, b, c, d, e);
        });
      case 6:
        return curry6(function(a, b, c, d, e, f){
          return fn(z0, z1, z2, a, b, c, d, e, f);
        });
      case 7:
        return curry7(function(a, b, c, d, e, f, g){
          return fn(z0, z1, a, b, c, d, e, f, g);
        });
      case 8:
        return curry8(function(a, b, c, d, e, f, g, h){
          return fn(z0, a, b, c, d, e, f, g, h);
        });
      }
    }
  };
};
//# sourceMappingURL=curry.js.map
