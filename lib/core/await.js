var α, $, flatten, toString$ = {}.toString;
α = require('./async');
module.exports = $ = function(f){
  var tasks, out;
  tasks = [];
  out = function(cb){
    if (out.finished) {
      if (cb.length === 2) {
        return $(function(done){
          cb(out.value, done);
        });
      } else {
        cb(out.value);
      }
    } else {
      if (cb.length === 2) {
        return $(function(done){
          out(function(val){
            return cb(val, done);
          });
        });
      } else {
        tasks.push(cb);
      }
    }
  };
  out.value = null;
  out.finished = false;
  out.async = true;
  α(function(){
    f(function(value){
      flatten(value, function(it){
        out.value = it;
        out.finished = true;
        while (tasks.length > 0) {
          tasks.shift()(out.value);
        }
      });
    });
  });
  return out;
};
flatten = function(value, done){
  switch (false) {
  case !(value != null && toString$.call(value.value).slice(8, -1) === 'Function'):
    α(function(){
      flatten(value.value, done);
    });
    break;
  case !(toString$.call(value).slice(8, -1) === 'Function' && value.async != null):
    value(function(it){
      flatten(it, done);
    });
    break;
  default:
    done(value);
  }
};
//# sourceMappingURL=await.js.map
