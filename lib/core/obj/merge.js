var keys, unique;
keys = require('./keys');
unique = require('../Array/unique');
module.exports = curry$(function(l, r){
  var bKeys, i$, len$, k, resultObj$ = {};
  bKeys = unique(keys(l).concat(keys(r)));
  for (i$ = 0, len$ = bKeys.length; i$ < len$; ++i$) {
    k = bKeys[i$];
    resultObj$[k] = l[k] || r[k];
  }
  return resultObj$;
});
function curry$(f, bound){
  var context,
  _curry = function(args) {
    return f.length > 1 ? function(){
      var params = args ? args.concat() : [];
      context = bound ? context || this : this;
      return params.push.apply(params, arguments) <
          f.length && arguments.length ?
        _curry.call(context, params) : f.apply(context, params);
    } : f;
  };
  return _curry();
}
//# sourceMappingURL=merge.js.map
