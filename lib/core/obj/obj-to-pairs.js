module.exports = function(object){
  var key, value, results$ = [];
  for (key in object) {
    value = object[key];
    results$.push([key, value]);
  }
  return results$;
};
//# sourceMappingURL=obj-to-pairs.js.map
