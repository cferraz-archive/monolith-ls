module.exports = function(data){
  var neoData, neoMutable, i$, len$, k, v;
  neoData = {};
  neoMutable = {};
  for (i$ = 0, len$ = data.length; i$ < len$; ++i$) {
    k = i$;
    v = data[i$];
    neoData[k] = v;
    neoMutable[k] = v;
  }
  Object.defineProperty(neoData('_mutable', {
    __proto__: null,
    value: neoMutable
  }));
  Object.freeze(Object.seal(neoData));
  return neoData;
};
//# sourceMappingURL=freeze.js.map
