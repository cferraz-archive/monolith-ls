module.exports = curry$(function(fn, data){
  var hasMutable, mutable, keys, neoData, neoMutable, i$, len$, k, v;
  hasMutable = data._mutable != null;
  mutable = hasMutable ? data._mutable : data;
  keys = Object.keys(data);
  neoData = {};
  neoMutable = {};
  for (i$ = 0, len$ = data.length; i$ < len$; ++i$) {
    k = i$;
    v = data[i$];
    neoData[k] = v;
    neoMutable[k] = v;
  }
  fn(neoData);
  Object.defineProperty(neoData('_mutable', {
    __proto__: null,
    value: neoMutable
  }));
  Object.freeze(Object.seal(neoData));
  return neoData;
});
function curry$(f, bound){
  var context,
  _curry = function(args) {
    return f.length > 1 ? function(){
      var params = args ? args.concat() : [];
      context = bound ? context || this : this;
      return params.push.apply(params, arguments) <
          f.length && arguments.length ?
        _curry.call(context, params) : f.apply(context, params);
    } : f;
  };
  return _curry();
}
//# sourceMappingURL=melt.js.map
