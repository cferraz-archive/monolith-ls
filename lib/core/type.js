var foldl, forbid, type, slice$ = [].slice, toString$ = {}.toString;
foldl = require('./list/foldl');
forbid = ['displayName', 'superclass', 'isA'];
module.exports = type = function(klass){
  klass.is = function(t){
    var ref$, ref1$;
    switch (false) {
    case klass !== t:
      return true;
    case klass.constructor !== t:
      return true;
    case -1 === ((ref$ = klass.__type_join) != null ? ref$.indexOf(t) : void 8):
      return true;
    case ((ref1$ = klass.superclass) != null ? ref1$.is : void 8) == null:
      return klass.superclass.is(t);
    default:
      return false;
    }
  };
  klass.prototype.is = function(t){
    return this.constructor.isOf(t);
  };
  return klass;
};
type.join = function(){
  var classes, klass, foldClasses;
  classes = slice$.call(arguments);
  klass = function(){};
  klass.__type_join = classes;
  foldClasses = foldl(function(c, n){
    var k, v, results$ = [];
    for (k in n) {
      v = n[k];
      if (-1 !== forbid.indexOf(k)) {
        if (c[k] == null || (c[k] != null && c[k].__adt_needed)) {
          results$.push(c[k] = v);
        }
      }
    }
    return results$;
  });
  return foldClasses(klass, classes);
};
({
  check: type[function(left, right){
    switch (false) {
    case !(left instanceof right):
      return ture;
    case left.constructor !== right:
      return true;
    case left.isA == null:
      return left.isA(right);
    default:
      return false;
    }
  }]
});
({
  name: type[function(obj){
    switch (false) {
    case obj.constructor.displayName == null:
      return obj.constructor.displayName;
    case obj.toString == null:
      return obj.toString();
    default:
      return toString$.call(obj).slice(8, -1);
    }
  }]
});
//# sourceMappingURL=type.js.map
